#include <stdio.h>
#include <windows.h>
#include <vector>
#include <algorithm>

BOOL IsProperWindow(int textLen, LONG right, HWND hWnd, DWORD dwStyle, DWORD dwExStyle) {
  return
    (0 < textLen)
    && (0 < right)
    && IsWindowVisible(hWnd)
    && IsWindowEnabled(hWnd)
    && (dwStyle & WS_CAPTION)
    && !(dwExStyle & 0x00200000L)
    ;
}

BOOL CALLBACK EnumWindowsProc(_In_ HWND hWnd, _In_ LPARAM lParam) {
  char               lpString[100];
  int                len;
  WINDOWINFO         wi;
  LONG               rightPos;
  DWORD              dwStyle;
  DWORD              dwExStyle;
  std::vector<HWND> *winList;

  len = GetWindowTextA(hWnd, lpString, 100);

  wi.cbSize = sizeof(wi);
  GetWindowInfo(hWnd, &wi);
  rightPos = wi.rcWindow.right;
  dwStyle = wi.dwStyle;
  dwExStyle = wi.dwExStyle;

  winList = reinterpret_cast<std::vector<HWND> *>(lParam);
  if (IsProperWindow(len, rightPos, hWnd, dwStyle, dwExStyle)) {
    winList->push_back(hWnd);
  }

  return true;
}

RECT GetScreenSize() {
  RECT workArea;

  SystemParametersInfoA(SPI_GETWORKAREA, 0, &workArea, 0);

  return workArea;
}

int main() {
  std::vector<HWND> winList;
  int               numWins;
  RECT              screenSize;
  int               w, h;
  int               x, y;
  int               dx, dy;

  FreeConsole();

  EnumWindows(EnumWindowsProc, reinterpret_cast<LPARAM>(&winList));
  numWins = winList.size();
  printf("numWins = %d\n", numWins);

  screenSize = GetScreenSize();
  printf("width, height = %ld, %ld\n", screenSize.right, screenSize.bottom);

  w = (screenSize.right * std::max(2, numWins) / (numWins + 1));
  h = (screenSize.bottom * std::max(2, numWins) / (numWins + 1));
  printf("w, h = %d, %d\n", w, h);

  dx = ((screenSize.right - w) / std::max(1, (numWins - 1)));
  dy = ((screenSize.bottom - h) / std::max(1, (numWins - 1)));
  printf("dx, dy = %d, %d\n", dx, dy);

  x = 0;
  y = 0;
  for (const auto& win: winList) {
    MoveWindow(win, x, y, w, h, true);

    x += dx;
    y += dy;
  }
}
